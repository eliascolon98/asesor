
// $(".btn-registrar").click(function(){
//     let identificacion = $("#identificacion").val();
//     localStorage.setItem("identificacion", identificacion);
// })

$("#identificacion").change(function(){
    let identificacion = $("#identificacion").val();
    localStorage.setItem("identificacion", identificacion);
})

$(document).ready(function(){
    let plan = localStorage.getItem("plan");
    let nombre_plan = localStorage.getItem("nombre_plan");
    let precio = localStorage.getItem("precio");
    $("#plan").val(plan);
    $("#nombre_plan").val(nombre_plan);
    $("#precio").val(precio);
})

$.ajax({
    url: 'https://acontis.biinyugames.com/backend_asesor/consultasJSON.php?action=TIPO_DOC',
    dataType: 'JSON',
    type: 'POST',
    success: function(d){
        var doc = '';
        for(var i = 0; i < d.length; i++){
            doc += `<option value="${d[i].id}">${d[i].descripcion}</option>`;
        }
        $("#tipo_documento").append(doc);
    }
})

$(document).ready(function(){
    var fullUrlPlan = window.location.href;
    var urlComponentsPlans = fullUrlPlan.split("=");
    var miPlan = urlComponentsPlans[urlComponentsPlans.length - 1];
    for(var i = 0; i < 1000000; i++){
        if(miPlan == i){
            localStorage.setItem("plan", miPlan);
        }
    }
   
    

    var fullUrl = window.location.href;
    var urlComponents = fullUrl.split("#");
    var data = urlComponents[urlComponents.length - 1];
    if(data == "mailExiste"){
       alert("Este correo electrónico ya existe en nuestra base de datos");
       location.href = "registrar.html";
    }else if(data == "idExiste"){
        alert("Esta identifiación ya existe en nuestra base de datos");
        location.href = "registrar.html";
    }else if(data == "fail"){
        alert("ERROR al registrar usuario");
        location.href = "registrar.html";
    }
})

$(".btn-pagar").click(function(){
    location.href = "dashboard/index.php";
})