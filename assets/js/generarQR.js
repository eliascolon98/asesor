let usuario_mail = localStorage.getItem("usuario");
let data_emp = localStorage.getItem("data-emp");

$(document).ready(function () {

    $("#generarCodigo").on("click", function () {

        var info_emp = "";
        var id_emp = "";
        var cadena = $("#empresa_sel").val();

       
        if (data_emp == 1) {
            
            if (cadena == "") {
                alert("Seleccione una empresa");
                return false;
            } else {
                $("#generarCodigo").attr("disabled", "true");
                $("#generarCodigo").removeClass("bg3-acontis");
                $("#generarCodigo").addClass("btn-secondary");

                time = parseInt(21);
                var minutos = Math.floor(time / 60);
                var segundos = time - (minutos * 60);


                document.getElementById('time').innerHTML =
                    00 + minutos + ":" + segundos;
                startTimer();

                info_emp = $('select[name="empresa_sel"] option:selected').text();
                id_emp = $('select[name="empresa_sel"] option:selected').value();
            }


        } else if (data_emp == 2){

            $("#generarCodigo").attr("disabled", "true");
            $("#generarCodigo").removeClass("bg3-acontis");
            $("#generarCodigo").addClass("btn-secondary");

            time = parseInt(21);
            var minutos = Math.floor(time / 60);
            var segundos = time - (minutos * 60);


            document.getElementById('time').innerHTML =
                00 + minutos + ":" + segundos;
            startTimer();

            id_emp = localStorage.getItem("id_emp");
            info_emp = $("#empresa").val();
        }

        makeCode(id_emp, info_emp);



    });

});

$("#fecha-limite").change(function () {
    $("#fecha-limite").val();

})
function makeCode(id_emp, info_emp) {

    // var elText = document.getElementById("empresa");
    // var texto = elText.value;
    $.ajax({
        url: 'https://codigosqracontis.biinyugames.com/codigoQR.php',
        dataType: 'html',
        type: 'POST',
        data: { texto: id_emp, nombre_emp: info_emp, caducidad: 'no' },
        success: function (data) {
            $("#codigoQR").html(data)
            var url_codigo = $(".img-codigo").attr("src");
            // console.log(url_codigo)
            setTimeout(() => {
                $.ajax({
                    url: 'https://acontis.biinyugames.com/backend_asesor/codigoQR.php',
                    dataType: 'html',
                    type: 'POST',
                    data: { usuario: usuario_mail, url_codigo: url_codigo, texto: info_emp, caducidad: 'no' },
                    success: function (data) {
                        // console.log(data);
                    }
                })
            }, 500);

        }
    })

    // qrcode.makeCode(elText.value);
}


function codigoCaducado() {

    var texto = 'Este codigo ya no existe.';
    localStorage.setItem("codigo_caduco", "0");
    $.ajax({
        url: 'https://codigosqracontis.biinyugames.com/codigoQR.php',
        dataType: 'html',
        type: 'POST',
        data: { texto: texto, caducidad: 'si' },
        success: function (data) {
            $("#codigoQR").html(data);
            var url_codigo = $(".img-codigo").attr("src");
            setTimeout(() => {
                $.ajax({
                    url: 'https://acontis.biinyugames.com/backend_asesor/codigoQR.php',
                    dataType: 'html',
                    type: 'POST',
                    data: { usuario: usuario_mail, url_codigo: url_codigo, texto: texto, caducidad: 'si' },
                    success: function (data) {
                        // console.log(data)
                    }
                })
            }, 500);

        }
    })

}




function startTimer() {

    var presentTime = document.getElementById('time').innerHTML;


    var timeArray = presentTime.split(/[:]+/);
    var m = timeArray[0];
    var s = checkSecond((timeArray[1] - 1));

    if (s == 59) { m = m - 1 }
    if (m < 0) {
        $("#generarCodigo").removeAttr("disabled");
        $("#generarCodigo").removeClass("btn-secondary");
        $("#generarCodigo").addClass("bg3-acontis");

        codigoCaducado()
        return false;
    }
    timeScore = presentTime;

    document.getElementById('time').innerHTML = m + ":" + s;

    setTimeout(startTimer, 1000);
}

function checkSecond(sec) {
    if (sec < 10 && sec >= 0) { sec = "0" + sec };
    if (sec < 0) { sec = "59" };
    return sec;
}



if (data_emp == 1) {
    $("select[name=empresa_sel]").change(function () {
        var option = $('select[name=empresa_sel]').val();
        localStorage.setItem("id_emp", option)
    });
} else if (data_emp == 2){
    info_emp = $("#empresa").val();
    localStorage.setItem("id_emp", info_emp)
}