
function aleatorio(inferior, superior) {
    var numPosibilidades = superior - inferior;
    var aleatorio = Math.random() * (numPosibilidades + 1);
    aleatorio = Math.floor(aleatorio);
    return inferior + aleatorio;
}

$(".btn-subirDoc").click(function () {

    let btn_enviar = $(".btn-subirDoc");
    let nombre = $("#nombreDoc").val();
    let numRandon = aleatorio(1, 999999999);
    let imagen_cam = document.getElementById('smallImage');
    window.scrollTo(0, 0);
    html2canvas(imagen_cam).then(function (canvas) {

        imagen = canvas.toDataURL("image/jpeg", 0.9);


        if (nombre == "") {
            alert("Debe colocarle algun nombre");
            return false;
        }
        let nombreFormateado = numRandon + '_' + nombre;
     

        $.ajax({
            url: "https://acontis.biinyugames.com/backend_asesor/asesorJSON.php?action=ARCHIVOS",
            type: 'POST',
            data: { nombre: nombreFormateado, usuario: usuario },
            dataType: 'html',
            type: 'POST',
            beforeSend: function (xhr) {
                btn_enviar.text("Procesando...");

            },
            success: function (data) {
                console.log(data);
          
                if (data == "1[]") {
                    $.ajax({
                        url: "https://codigosqracontis.biinyugames.com/generarDoc.php",
                        type: 'POST',
                        data: { nombre: nombreFormateado, usuario: usuario, imagen: imagen },
                        dataType: 'html',
                        type: 'POST',
                        success: function (data) {

                            if (data == "1") {
                                alert("Documento guardado con exito");
                                btn_enviar.text("Subir");
                                $("#nombreDoc").val("");

                            } else {
                                alert("Error");
                            }

                        },
                    });

                } else {
                    alert("Error");
                }

            },
        });

    })

})