$(document).ready(function () {
    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/API_INFO_JSON.php?action=SESION',
        dataType: 'json',
        type: 'GET',
        global: true,
        async: false,
        success: function (data) {
            const token = data['access_token'];
            localStorage.setItem("token", token)
            // EMPRESAS
            $.ajax({
                url: 'https://acontis.biinyugames.com/backend_asesor/API_INFO_JSON.php?action=EMPRESAS',
                dataType: 'json',
                data: { token: token },
                type: 'POST',
                success: function (empresas) {

                    function BuscarEmp(empresa) {
                        return empresa.name_company === 'Edificio Trocadero Propiedad Horizontal';
                    }

                    let emp_info = empresas.find(BuscarEmp);

                    let direccion = emp_info['address_company'];
                    let ciudad = emp_info['ciudad'];
                    let email_company = emp_info['email_company'];
                    let id_company = emp_info['id_company'];
                    let name_company = emp_info['name_company'];
                    let nit_company = emp_info['nit_company'];
                    let representante_legal = emp_info['representante_legal'];
                    let servicio = emp_info['servicio'];
                    let telephone_company = emp_info['telephone_company'];
                    let tipo_cliente = emp_info['tipo_cliente'];
                    // console.log(nit_company);
                }
            })





        }
    })
    const id_emp = localStorage.getItem("id_emp");
    $("#url-certificacion").attr("href", "https://crm.acontis.co/referencia-comercial/" + id_emp);
    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/asesorJSON.php?action=DOCUMENTOS',
        dataType: 'json',
        type: 'POST',
        data: { id_emp: id_emp },
        success: function (datos) {
            var periodos = "";
            for (var i = 0; i < datos.length; i++) {
                periodos += `<div class="contenido-periodos mt-3 ">
                                <div class="d-flex" onclick="abrirDocumentos(${datos[i].id_periodo})">
                                    <div class="col-10">
                                        <h4 class="mb-0"><b>${datos[i].nombre_periodo}</b> </h4>
                                    </div>
                                    <div class="col-2 d-flex jurtifiy-content-end ">
                                        <i class="fas fa-folder pt-2" aria-hidden="true" style="font-size: 2rem; color:#dddfeb"></i>
                                    </div>
                                </div>
                             </div>`;
            }
            $(".row-periodos").html(periodos);
            console.log(datos);
        }
    })
    
})
function abrirDocumentos(id_periodo){
    $(".regresar-infocon-2").addClass("d-none");
    $(".regresar-infocon").removeClass("d-none");
    $(".row-periodos").addClass("d-none");
    $(".row-documentos").removeClass("d-none");

    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/asesorJSON.php?action=DOCUMENTO_PERIODO',
        dataType: 'json',
        type: 'POST',
        data: { id_periodo: id_periodo },
        success: function (datos) {
            var periodos = "";
            for (var i = 0; i < datos.length; i++) {
                periodos += `<div class="contenido-periodos mt-3 ">
                                <a href="https://crm.acontis.co/documentoperiodo/${datos[i].url_documento}" target="blank">
                                <div class="d-flex" >
                                    <div class="col-10">
                                        <h4 class="mb-0 pt-2"><b>${datos[i].nombre_documento}</b> </h4>
                                    </div>
                                    <div class="col-2 d-flex jurtifiy-content-end ">
                                    <i class="fas fa-file-pdf pt-2" aria-hidden="true" style="font-size: 2rem; color:#dddfeb"></i>
                                    </div>
                                </div>
                                </a>
                             </div>`;
            }
            
            $(".row-documentos").html(periodos);
        }
    })
}

$(".regresar-infocon").click(function(){
    $(".regresar-infocon-2").removeClass("d-none");
    $(".regresar-infocon").addClass("d-none");
    $(".row-periodos").removeClass("d-none");
    $(".row-documentos").addClass("d-none");
})

$("select[name=infoCuenta]").change(function () {
    const token_validar = localStorage.getItem("token");
    const id_emp = localStorage.getItem("id_emp");
    var option = $('select[name=infoCuenta]').val();
    var accion = "";



    if (option == "cuentas-por-cobrar") {

        $(".table-cuentaCobro").removeClass("d-none");
        $(".table-cuentasProveedores").addClass("d-none");
        $(".table-ope").addClass("d-none");


        // CUENTAS POR COBRAR A TRABAJADORES
        $.ajax({
            url: 'https://acontis.biinyugames.com/backend_asesor/API_INFO_JSON.php?action=CUENTACOBRO',
            dataType: 'json',
            data: { token: token_validar, id_emp: id_emp },
            type: 'POST',
            success: function (cuenta) {

                console.log(cuenta)
                var info_cuenta = '';
                for (x = 0; x < cuenta.total; x++) {
                    var str = cuenta.data[x].Saldo;
                    var n = str.indexOf(".");

                    var str_remplazar = cuenta.data[x].Saldo;
                    var nuevo_saldo = str_remplazar.substr(0, n);

                    info_cuenta += `
                    <tr>
                        <td>${cuenta.data[x].Nombres_terceros}</td>
                        <td>${nuevo_saldo}</td>
                    </tr>
                    `;

                }
                $("#cuentasPorPagar").html(info_cuenta);

            }
        })

    } else if (option == "cuentas-por-pagar") {

        $(".table-cuentaCobro").removeClass("d-none");
        $(".table-cuentasProveedores").addClass("d-none");
        $(".table-ope").addClass("d-none");


        // CUENTAS POR PAGAR
        $.ajax({
            url: 'https://acontis.biinyugames.com/backend_asesor/API_INFO_JSON.php?action=CUENTAPAGOS',
            dataType: 'json',
            data: { token: token_validar, id_emp: id_emp },
            type: 'POST',
            success: function (cuenta) {

                // console.log(cuenta.data[1].Nombres_terceros)
                var info_cuenta = '';
                for (x = 0; x < cuenta.total; x++) {
                    var str = cuenta.data[x].Saldo;
                    var n = str.indexOf(".");

                    var str_remplazar = cuenta.data[x].Saldo;
                    var nuevo_saldo = str_remplazar.substr(0, n);

                    info_cuenta += `
                    <tr>
                        <td>${cuenta.data[x].Nombres_terceros}</td>
                        <td>${nuevo_saldo.toLocaleString("es-ES")}</td>
                    </tr>
                    `;

                }
                $("#cuentasPorPagar").html(info_cuenta);

            }
        })
    } else if (option == "cuentas-por-pagar-a-proveedores") {

        $(".table-cuentasProveedores").removeClass("d-none");
        $(".table-cuentaCobro").addClass("d-none");
        $(".table-ope").addClass("d-none");

    } else if (option == "ingresos-operacionales") {

        $(".titulo-op").text("DATOS OPERACIONALES")

        $(".table-cuentasProveedores").addClass("d-none");
        $(".table-cuentaCobro").addClass("d-none");
        $(".table-ope").removeClass("d-none");



    } else if (option == "ingresos-no-operacionales") {

        $(".titulo-op").text("DATOS NO OPERACIONALES")

        $(".table-cuentasProveedores").addClass("d-none");
        $(".table-cuentaCobro").addClass("d-none");
        $(".table-ope").removeClass("d-none");

    }

    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/API_INFO_JSON.php?action=INFOCONTABLE',
        dataType: 'json',
        data: { token: token_validar, codigo: option, id_emp: id_emp },
        type: 'POST',
        success: function (cuenta) {
            var info_cuenta = '';
            // console.log(cuenta);

            if (cuenta.data == "") {
                $(".info-null").removeClass("d-none");
            }

            if (option == "cuentas-por-pagar-a-proveedores") {

            } else if (option == "ingresos-operacionales" || option == "ingresos-no-operacionales") {

                for (x = 0; x < cuenta.data.length; x++) {


                    info_cuenta += `
                    <table class="table table-light table-bordered table-striped ">
                                           
                        <tbody>
                            <tr>
                                <td>Empresa: </td>
                                <td>${cuenta.data[x].Empresa}</td>
                                
                            </tr>
                            <tr>
                                <td>Nombre Documento: </td>
                                <td>${cuenta.data[x].Nombre_Documento}</td>
                                
                            </tr>
                            <tr>
                                <td>Tercero: </td>
                                <td>${cuenta.data[x].Tercero}</td>
                                
                            </tr>
                            <tr>
                                <td>Empleado: </td>
                                <td>${cuenta.data[x].Empleado}</td>
                                
                            </tr>
                            <tr>
                                <td>Crédito: </td>
                                <td>${cuenta.data[x].Crédito}</td>
                                
                            </tr>
                            <tr>

                            <td>Cod. Cuenta: </td>
                            <td>${cuenta.data[x].Codigo_Cuenta}</td>
                            
                            </tr>

                            <tr>
                                <td>Nombre Cuenta: </td>
                                <td>${cuenta.data[x].Nombre_Cuenta}</td>
                                
                            </tr>
                            <tr>
                                <td>Estado: </td>
                                <td>${cuenta.data[x].SenAprobado}</td>
                                
                            </tr>
                          
                        </tbody>

                    </table>`;

                }
                $(".div-opeacionales").html(info_cuenta);

            }


        }
    })
});


