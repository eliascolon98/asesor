let nombre, correo, telefono, nit, foto_bdm;
let usuario = localStorage.getItem("usuario");
let num_alertas = localStorage.getItem("num_alertas")
let menu_option_contable = '';
var display_none = "";
$(document).ready(function () {

    $('.js-example-basic-single').select2();
    $(".num_msg").text(num_alertas);

    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/asesorJSON.php?action=INFO_EMPRESA',
        dataType: 'JSON',
        type: 'POST',
        global: true,
        async: false,
        data: { usuario: usuario },
        beforeSend: function () {
            // console.table(usuario)
        },
        success: function (data) {
            //  console.table(data)
            var empresa_sel = "";
          
            if (data[0].servicio == "Revisoria Fiscal") {
                menu_option_contable = "Mis dictamenes/informes";
                display_none = 'style="display: none"';
            } else {
                menu_option_contable = "Mi información contable";
            }
            if (data.length > 1) {
                $("#empresa_sel").removeClass("d-none");
                $("#empresa").addClass("d-none");
                localStorage.setItem("data-emp", 1);
                for (var x = 0; x < data.length; x++) {
                    empresa_sel += `<option value="${data[x].id_company}"> ${data[x].nombre}</option>`;
                }
                $("#empresa_sel").append(empresa_sel);
            } else {
                localStorage.setItem("data-emp", 2);
                $("#empresa").removeClass("d-none");
                $("#empresa_sel").addClass("d-none");
                $("#empresa").val(data[0].nombre);
                localStorage.setItem("id_emp", data[0].id_company)
            }


        }
    })
    // $(".user_avatar").attr("src", foto_bd)

    //confirguracion personal
    const configurar = `<div class="list-group mt-3 shadow">
    <a href="#" onclick="EditarPerfil()" class="list-group-item list-group-item-action ">
        <i class="mr-2 icon-user light-green-text"></i>Mi perfil
    </a>
  
    <a href="#" onclick="cambiarPass('${correo}')" class="list-group-item list-group-item-action"><i
            class="mr-2 icon-unlock-alt light-green-text"></i>Cambiar contraseña</a>
    </div>`;




    //Menú usuario
    const menu_user = ` <li class="header"><strong>Navegación</strong></li>
    <li class="treeview">
        <a href="index.html"><i class="icon icon-home2 light-green-text s-18">
        </i>Inicio</a>
    </li>

    <li class="treeview" ${display_none}>
        <a href="estadoCuenta.html"><i class="icon icon-list-ul light-green-text s-18">
        </i>Información contable</a>
    </li>
    <li class="treeview">
        <a href="miInformacionConrable.html"><i class="icon icon-list-ul light-green-text s-18">
        </i>${menu_option_contable}</a>
    </li>
    
    <li class="treeview">
        <a href="certificaciones.html"><i class="icon icon-list-ul light-green-text s-18">
        </i>Certificaciones</a>
    </li>

    <li class="treeview">
        <a href="../../subirArchivo.html"><i class="icon icon-plus-circle light-green-text s-18">
        </i>Subir documentos</a>
    </li>
    <li class="treeview">
        <a href="#" onclick="enviarCorreo()"><i class="icon icon-plus-circle light-green-text s-18">
        </i>Solicitar asistencia especializada</a>
    </li>

    <li class="treeview">
         <a  onclick="CerrarSesion()"><i class="icon icon-sign-out light-green-text s-18">
        </i>Cerrar sesión</a>
    </li>`;

    $("#menu-user").html(menu_user);
    $("#userSettingsCollapse").html(configurar);

    //Menú vemtana subir documentos
    const menu_user2 = ` <li class="header"><strong>Navegación</strong></li>
     <li class="treeview">
         <a href="asesor/vistas/index.html"><i class="icon icon-home2 light-green-text s-18">
         </i>Inicio</a>
     </li>

     <li class="treeview" ${display_none}>
        <a href="asesor/vistas/estadoCuenta.html"><i class="icon icon-list-ul light-green-text s-18">
        </i>Información contable</a>
    </li>

    <li class="treeview">
        <a href="asesor/vistas/miInformacionConrable.html"><i class="icon icon-list-ul light-green-text s-18">
        </i>${menu_option_contable}</a>
    </li>
    <li class="treeview">
        <a href="asesor/vistas/certificaciones.html"><i class="icon icon-list-ul light-green-text s-18">
        </i>Certificaciones</a>
    </li>

    <li class="treeview">
        <a href="subirArchivo.html"><i class="icon icon-plus-circle light-green-text s-18">
        </i>Subir documentos</a>
    </li>
    <li class="treeview">
        <a href="#" onclick="enviarCorreo()"><i class="icon icon-plus-circle light-green-text s-18">
        </i>Solicitar asistencia especializada</a>
    </li>
 
     <li class="treeview">
     <a  onclick="CerrarSesion()"><i class="icon icon-sign-out light-green-text s-18">
         </i>Cerrar sesión</a>
     
 </li>`;

    $("#menu-user2").html(menu_user2);

})
function CerrarSesion() {
    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/cerrarSesion.php',
        dataType: 'html',
        type: 'POST',
        data: { usuario: usuario },
        success: function (data) {
            // console.log(data);
            if (data == 1) {
                localStorage.setItem("sesion", "I")
                location.href = "../login/login.html";
            } else if (data == "error") {
                alert("Error cerrar sesión.")
            }
        }
    })

}

function cambiarPass(email) {

    $("#FormcambiarPass").removeClass("dis-n")
    $("#formUsuarioEditar").addClass("dis-n")
    $(".titulo-modal-usuario").text("CAMBIAR CONTRASEÑA");
    $(".btn-cambiar-pass").removeClass("dis-n")
    $(".btn-editar-usuario").addClass("dis-n")
    $("#modal-editar-usuario").modal("show")
    $("#email-pass").val(email)



}
$(".btn-cambiar-pass").click(function () {


    let pass = $("#passEditar").val();
    let confirmar_pass = $("#confirmarPass").val();

    if (pass == "") {
        alert("Debe escribir una contraseña.")
        return false;
    } else if (confirmar_pass == "") {
        alert("Debe confirmar la contraseña.")
        return false;
    } else if (confirmar_pass != pass) {
        alert("Las contraseñas no coinciden.")
        return false;
    } else {
        $.ajax({
            url: 'https://acontis.biinyugames.com/backend_asesor/updateProfile.php',
            dataType: 'html',
            type: 'POST',
            data: { pass: pass },
            success: function (data) {

                if (data == 1) {
                    alert("Contraseña cambiada con éxito.")
                    $(".close").trigger("click");
                    $("#email-pass").val("");
                    $("#passEditar").val("");
                } else if (data == "error") {
                    alert("Error al cambiar contrase{a}.")
                }
            }
        })
    }
})

function EditarPerfil() {

    $(".titulo-modal-usuario").text("EDITAR DATOS DE LA EMPRESA");
    $("#FormcambiarPass").addClass("dis-n")
    $("#formUsuarioEditar").removeClass("dis-n")
    $(".btn-cambiar-pass").addClass("dis-n")
    $(".btn-editar-usuario").removeClass("dis-n")

    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/asesorJSON.php?action=INFO_EMPRESA',
        dataType: 'JSON',
        type: 'POST',
        global: true,
        async: false,
        data: { usuario: usuario },
        success: function (data) {

            $("#nombre-user").html(data[0].nombre);

            $("#nombre-empresa").val(data[0].nombre);
            $("#nit").val(data[0].nit_company);
            $("#telefono").val(data[0].telefono);
            $("#direccion").val(data[0].direccion);
            $("#email-empresa").val(data[0].correo);
        }
    })



    $("#modal-editar-usuario").modal("show")
}

$(".btn-editar-usuario").click(function () {
    let datos = $("#formUsuarioEditar").serialize();
    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/updateProfile.php',
        dataType: 'html',
        type: 'POST',
        data: datos,
        success: function (data) {
            if (data == 1) {
                alert("Editado con éxito.");
                EditarPerfil()
            } else if (data == 2) {
                alert("Error al editar usuario.")
            }
        }
    })

})


$.ajax({
    url: 'https://acontis.biinyugames.com/backend_asesor/asesorJSON.php?action=ALERTAS',
    dataType: 'JSON',
    type: 'POST',
    data: { usuario: usuario },
    success: function (data) {
        //  console.table(data)
        var alert = '';
        var num_msg = 0;
        for (var i = 0; i < data.length; i++) {
            num_msg++;
            alert += `<div class="MensajeTiendaNew alert alert-success">
                <p class="msg-alert ">${data[i].alerta}</p>
                <p class="">${data[i].fecha}</p>
            </div>`;

        }
        localStorage.setItem("num_alertas", num_msg)
        $("#alertas").html(alert);
    }
})

function enviarCorreo() {
    $("#modalCorreo").modal("show")
}
$(".btn-confirmar-asistencia").click(function () {
    var servicio = $("#servicio").val();
    var emp = localStorage.getItem("id_emp");
    $.ajax({
        url: 'https://acontis.biinyugames.com/backend_asesor/mailAsistenciaEsp.php',
        dataType: 'html',
        type: 'POST',
        data: { emp: emp, servicio: servicio },
        beforeSend: function () {
            $(".btn-confirmar-asistencia").addClass("d-none");
            $(".btn-cancelar-asistencia").addClass("d-none");
            $("#servicio").addClass("d-none");
            $(".mail-des").text("Por favor espere...");
            if(servicio == ""){
                alert("Debe selecciona una opción");
                return false;
            }
        },
        success: function (data) {
            if (data == 1) {
                
                $(".modal-title").text("Correo enviado con éxito");
                $(".mail-des").text("Solicitud de  asistencia conespecializada enviada con éxito.");
                $(".fa-check-circle").removeClass("d-none")
                setTimeout(() => {
                    location.reload();
                }, 3000);
            } else if (data == 2) {
                $(".modal-title").text("Error");
                $(".mail-des").text("Error al enviar correo");
            }


        }
    })
})
$("#logout").click(function () {
    localStorage.setItem("sesion", "I");
})
$(document).ready(function(){
    setTimeout(() => {
        $(".loader-fade").addClass("loader-fade");
    }, 1000);
})

