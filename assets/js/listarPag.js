// peticion ajax enviada como callback
$(document).ready(function() {
    $(buscar_datos());
    // se genera el paginador
    paginador = $(".pagination");
    // cantidad de items por pagina
    var items = 6,
        numeros = 6;
    // inicia el paginador
    init_paginator(paginador, items, numeros);
    // se envia la peticion ajax que se realizara como callback
    set_callback(buscar_datos);
    cargaPagina(0);

    function buscar_datos(consulta) {

        $.ajax({
            type: "POST",
            data: {
                consulta: consulta,
                limit: itemsPorPagina,
                offset: desde
            },
            url: "https://acontis.biinyugames.com/backend_asesor/paginador.php"
        }).done(function(data, textStatus, jqXHR) {

            // obtiene la clave lista del json data
            var lista = data.lista;
            $("#tabla").html("");

            // si es necesario actualiza la cantidad de paginas del paginador
            if (pagina == 0) {
                creaPaginador(data.cantidad);
            }

            var salida = "<table class='align-middle mb-0 table table-borderless" +
                "table-striped table-hover'>" +
                "<thead class='emp-th'>" +
                "<th class='text-center'>ID</th>" +
                "<th class='text-center'>DESCRIPCIÓN</th>" +
                "<th class='text-center'>SERVICIO</th>" +
                "<th class='text-center'>FECHA</th>" +
                "<th class='text-center'>SEDE</th>" +
                "<th class='text-center borrar-cita'>BORRAR</th>" +
                "</thead>" +
                "<tbody class='rows-user'>"
            "</tbody>";
            $(salida).appendTo("#tabla");

            // genera el cuerpo de la tabla
            $.each(lista, function(ind, elem) {

                $('<tr>' +

                    '<td class="text-center text-muted">' + elem.id + '</td>' +
                    '<td>' + elem.descripcion + '</td>' +
                    '<td class="text-center">' + elem.servicio + '</td>' +
                    '<td class="text-center">' + elem.fecha + ' - ' + elem.hora + '</td>' +
                    '<td class="text-center">' + elem.sede + '</td>' +
                    '<td class="text-center borrar-cita">' +
                    '<button type="button" onclick="borrarCita(' + elem.id + ')"' +
                    'class="btn btn-primary btn-sm" type="button" ' +
                    'data-toggle="modal" data-target="#confirm-detalle"><i class="fas fa-trash-alt"></i></button>' +
                    '</td>' +
                    '</tr>').appendTo($(".rows-user"));

            });

        }).fail(function(jqXHR, textStatus, textError) {
            alert("Error al realizar la peticion dame");
        });
    }

    $("#busqueda").change(function(event) {
        // cargaPagina(0);
        buscar();
    });
    $("#limpiar").click(function() {
        $("#busqueda").val("");
        buscar();
    })

    function buscar() {
        var valorBusqueda = $("#busqueda").val();
        if (valorBusqueda != "") {
            buscar_datos(valorBusqueda);
        } else {
            buscar_datos();
        }
    }
    $("select[name=horas_admin]").change(function() {
        var option = $('select[name=horas_admin]').val();
        if (option != "") {
            $("#busqueda").val(option)
        }
    });
})

$(".close").click(function() {
    $("#busqueda").val("");
})